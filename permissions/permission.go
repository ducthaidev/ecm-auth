package permissions

type Permission struct {
	id   *int64
	name string
}
