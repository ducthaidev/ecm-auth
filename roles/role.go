package roles

type Role struct {
	id   *int64
	name string
}

type RoleHierarchy struct {
	id       int64
	child_id int64
}
