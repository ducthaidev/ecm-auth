CREATE SCHEMA IF NOT EXISTS ecm_auth;

CREATE TABLE IF NOT EXISTS ecm_auth.users (
    id BIGSERIAL PRIMARY KEY,
    email VARCHAR(100) UNIQUE NOT NULL,
    username VARCHAR(100) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS ecm_auth.roles (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS ecm_auth.permissions (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS ecm_auth.users_roles (user_id BIGSERIAL, role_id BIGSERIAL);

CREATE TABLE IF NOT EXISTS ecm_auth.roles_permissions (
    role_id BIGSERIAL,
    permission_id BIGSERIAL
);

CREATE TABLE IF NOT EXISTS ecm_auth.role_hierarchies (id BIGSERIAL, child_id BIGSERIAL);

ALTER TABLE
    ecm_auth.users_roles DROP CONSTRAINT IF EXISTS users_roles_pkey;

ALTER TABLE
    ecm_auth.users_roles
ADD
    CONSTRAINT users_roles_pkey PRIMARY KEY (user_id, role_id);

ALTER TABLE
    ecm_auth.roles_permissions DROP CONSTRAINT IF EXISTS roles_permissions_pkey;

ALTER TABLE
    ecm_auth.roles_permissions
ADD
    CONSTRAINT roles_permissions_pkey PRIMARY KEY (role_id, permission_id);

ALTER TABLE
    ecm_auth.role_hierarchies DROP CONSTRAINT IF EXISTS role_hierarchies_pkey;

ALTER TABLE
    ecm_auth.role_hierarchies
ADD
    CONSTRAINT role_hierarchies_pkey PRIMARY KEY (id, child_id);